-- MySQL dump 10.14  Distrib 5.5.49-MariaDB, for Linux (i686)
--
-- Host: localhost    Database: analyzer-turboseo
-- ------------------------------------------------------
-- Server version	5.5.49-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(250) DEFAULT NULL,
  `pass` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','da37702156462803b5af89ac6a85dce5'),(2,'admin','da37702156462803b5af89ac6a85dce5'),(3,'admin','da37702156462803b5af89ac6a85dce5'),(4,'admin','da37702156462803b5af89ac6a85dce5'),(5,'admin','da37702156462803b5af89ac6a85dce5');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_history`
--

DROP TABLE IF EXISTS `admin_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_date` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_history`
--

LOCK TABLES `admin_history` WRITE;
/*!40000 ALTER TABLE `admin_history` DISABLE KEYS */;
INSERT INTO `admin_history` VALUES (1,'14th June 2014','23.54.2.43'),(2,'14th June 2014','26.32.34.33'),(3,'15th June 2014','31.7.42.03'),(4,'14th June 2014','23.54.2.43'),(5,'14th June 2014','26.32.34.33'),(6,'15th June 2014','31.7.42.03'),(7,'14th June 2014','23.54.2.43'),(8,'14th June 2014','26.32.34.33'),(9,'15th June 2014','31.7.42.03'),(10,'14th June 2014','23.54.2.43'),(11,'14th June 2014','26.32.34.33'),(12,'15th June 2014','31.7.42.03'),(13,'14th June 2014','23.54.2.43'),(14,'14th June 2014','26.32.34.33'),(15,'15th June 2014','31.7.42.03'),(16,'4th August 2016','127.0.0.1'),(17,'8th August 2016','14.162.168.224');
/*!40000 ALTER TABLE `admin_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads` (
  `id` int(255) DEFAULT NULL,
  `text_ads` mediumtext,
  `ads_1` mediumtext,
  `ads_2` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ads`
--

LOCK TABLES `ads` WRITE;
/*!40000 ALTER TABLE `ads` DISABLE KEYS */;
INSERT INTO `ads` VALUES (1,'<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>'),(1,'<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>'),(1,'<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>'),(1,'<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>'),(1,'<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>','<center>\r\n <div >\r\n<img class=\"imageres\" src=\"img/ad700.png\">\r\n</div>  <br /> <br /> </center>');
/*!40000 ALTER TABLE `ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ban_site`
--

DROP TABLE IF EXISTS `ban_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ban_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(255) DEFAULT NULL,
  `last_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ban_site`
--

LOCK TABLES `ban_site` WRITE;
/*!40000 ALTER TABLE `ban_site` DISABLE KEYS */;
INSERT INTO `ban_site` VALUES (1,'example.com','17th June 2014'),(2,'prothemes.biz','17th June 2014');
/*!40000 ALTER TABLE `ban_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ban_user`
--

DROP TABLE IF EXISTS `ban_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ban_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `last_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ban_user`
--

LOCK TABLES `ban_user` WRITE;
/*!40000 ALTER TABLE `ban_user` DISABLE KEYS */;
INSERT INTO `ban_user` VALUES (1,'2.2.2.2','17th June 2014');
/*!40000 ALTER TABLE `ban_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lang`
--

DROP TABLE IF EXISTS `lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lang` (
  `id` int(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lang`
--

LOCK TABLES `lang` WRITE;
/*!40000 ALTER TABLE `lang` DISABLE KEYS */;
INSERT INTO `lang` VALUES (0,'en.php'),(0,'en.php'),(0,'en.php'),(0,'en.php'),(0,'en.php');
/*!40000 ALTER TABLE `lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_view`
--

DROP TABLE IF EXISTS `page_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(255) DEFAULT NULL,
  `tpage` varchar(255) DEFAULT NULL,
  `tvisit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_view`
--

LOCK TABLES `page_view` WRITE;
/*!40000 ALTER TABLE `page_view` DISABLE KEYS */;
INSERT INTO `page_view` VALUES (1,'14th June 2014','9','1'),(2,'14th June 2014','9','1'),(3,'15th June 2014','14','1'),(4,'15th June 2014','14','1'),(5,'14th June 2014','9','1'),(6,'15th June 2014','14','1'),(7,'14th June 2014','9','1'),(8,'15th June 2014','14','1'),(9,'14th June 2014','9','1'),(10,'15th June 2014','14','1'),(11,'4th August 2016','1','1'),(12,'5th August 2016','78','1'),(13,'6th August 2016','5','1'),(14,'8th August 2016','87','2'),(15,'9th August 2016','8','2'),(16,'10th August 2016','1','1'),(17,'11th August 2016','18','1');
/*!40000 ALTER TABLE `page_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_date` varchar(255) DEFAULT NULL,
  `page_name` varchar(255) DEFAULT NULL,
  `page_title` mediumtext,
  `page_content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'17th June 2014','About','About US','<p><strong>Nothing to say</strong></p><br><br><br><br><br><br><br><br><br><br>');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_info`
--

DROP TABLE IF EXISTS `site_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_info` (
  `id` int(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `des` mediumtext,
  `keyword` mediumtext,
  `site_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `twit` varchar(4000) DEFAULT NULL,
  `face` varchar(4000) DEFAULT NULL,
  `gplus` varchar(4000) DEFAULT NULL,
  `ex_1` mediumtext,
  `ex_2` mediumtext,
  `ps_1` mediumtext,
  `ps_2` mediumtext,
  `ps_3` mediumtext,
  `ga` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_info`
--

LOCK TABLES `site_info` WRITE;
/*!40000 ALTER TABLE `site_info` DISABLE KEYS */;
INSERT INTO `site_info` VALUES (0,'Turbo SEO Analyzer - All-in-one SEO Tool','','seo,analyzer,woorank,alexa,google','FREE WEBSITE AUDIT TOOL','admin@prothemes.biz','https://twitter.com/','https://www.facebook.com/','https://plus.google.com/','Google.com','Bing.com','google.com','Bing.com','Apple.com','UA-'),(0,'Turbo SEO Analyzer - All-in-one SEO Tool','','seo,analyzer,woorank,alexa,google','FREE WEBSITE AUDIT TOOL','admin@prothemes.biz','https://twitter.com/','https://www.facebook.com/','https://plus.google.com/','Google.com','Bing.com','google.com','Bing.com','Apple.com','UA-'),(0,'Turbo SEO Analyzer - All-in-one SEO Tool','','seo,analyzer,woorank,alexa,google','FREE WEBSITE AUDIT TOOL','admin@prothemes.biz','https://twitter.com/','https://www.facebook.com/','https://plus.google.com/','Google.com','Bing.com','google.com','Bing.com','Apple.com','UA-'),(0,'Turbo SEO Analyzer - All-in-one SEO Tool','','seo,analyzer,woorank,alexa,google','FREE WEBSITE AUDIT TOOL','admin@prothemes.biz','https://twitter.com/','https://www.facebook.com/','https://plus.google.com/','Google.com','Bing.com','google.com','Bing.com','Apple.com','UA-'),(0,'Turbo SEO Analyzer - All-in-one SEO Tool','','seo,analyzer,woorank,alexa,google','FREE WEBSITE AUDIT TOOL','admin@prothemes.biz','https://twitter.com/','https://www.facebook.com/','https://plus.google.com/','Google.com','Bing.com','google.com','Bing.com','Apple.com','UA-');
/*!40000 ALTER TABLE `site_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemap`
--

DROP TABLE IF EXISTS `sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` text,
  `des` text,
  `date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemap`
--

LOCK TABLES `sitemap` WRITE;
/*!40000 ALTER TABLE `sitemap` DISABLE KEYS */;
INSERT INTO `sitemap` VALUES (1,'prothemes.biz','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(2,'prothemes.biz','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(3,'prothemes.biz','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(4,'google.com','This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(5,'google.com','This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(6,'google.com','This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(7,'prothemes.biz','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(8,'google.com','This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(9,'prothemes.biz','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of prothemes.biz is 0 years, 281 days. Overall site looking Good. According to the Alexa.com, this site has 287713 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(10,'google.com','This web site has Google PageRank 9 out of 10 maxmium and is listed in DMOZ. The age of prothemes.biz is 16 years, 323 days. Overall site looking Good. According to the Alexa.com, this site has 1 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2014-08-04'),(11,'bing.com','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of bing.com is 20 years, 192 days. Overall site looking Good. According to the Alexa.com, this site has 13 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2016-08-08'),(12,'apple.com','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of apple.com is 29 years, 171 days. Overall site looking Good. According to the Alexa.com, this site has 54 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2016-08-08'),(13,'whatismyfuture.net','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of whatismyfuture.net is 2 years, 336 days. Overall site looking Good. According to the Alexa.com, this site has No Rank rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2016-08-08'),(14,'vicoders.com','This web site has Google PageRank 0 out of 10 maxmium and is not listed in DMOZ. The age of vicoders.com is 1 year, 4 days. Overall site looking Okay. According to the Alexa.com, this site has 10908487 rank in the world wide web. Low Alexa rank indicates that the site is visited by a lot of visitors and most popular in the Internet.','2016-08-11');
/*!40000 ALTER TABLE `sitemap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitemap_options`
--

DROP TABLE IF EXISTS `sitemap_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemap_options` (
  `id` int(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `changefreq` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitemap_options`
--

LOCK TABLES `sitemap_options` WRITE;
/*!40000 ALTER TABLE `sitemap_options` DISABLE KEYS */;
INSERT INTO `sitemap_options` VALUES (1,'0.9','weekly'),(1,'0.9','weekly'),(1,'0.9','weekly'),(1,'0.9','weekly'),(1,'0.9','weekly');
/*!40000 ALTER TABLE `sitemap_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_history`
--

DROP TABLE IF EXISTS `user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_date` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_history`
--

LOCK TABLES `user_history` WRITE;
/*!40000 ALTER TABLE `user_history` DISABLE KEYS */;
INSERT INTO `user_history` VALUES (1,'14th June 2014','23.54.72.77','prothemes.biz'),(2,'14th June 2014','23.54.72.77','prothemes.biz'),(3,'14th June 2014','23.54.72.77','prothemes.biz'),(4,'14th June 2014','23.54.72.77','prothemes.biz'),(5,'14th June 2014','23.54.72.77','bluepay.com'),(6,'14th June 2014','23.54.72.77','bluepay.com'),(7,'14th June 2014','23.54.72.77','bluepay.com'),(8,'14th June 2014','23.54.72.77','bluepay.com'),(9,'14th June 2014','23.54.72.77','codecanyon.net'),(10,'14th June 2014','23.54.72.77','codecanyon.net'),(11,'14th June 2014','23.54.72.77','codecanyon.net'),(12,'14th June 2014','23.54.72.77','codecanyon.net'),(13,'14th June 2014','23.54.72.77','mobikwik.com'),(14,'14th June 2014','23.54.72.77','mobikwik.com'),(15,'14th June 2014','23.54.72.77','mobikwik.com'),(16,'14th June 2014','23.54.72.77','mobikwik.com'),(17,'14th June 2014','23.54.72.77','interserver.net'),(18,'14th June 2014','23.54.72.77','interserver.net'),(19,'14th June 2014','23.54.72.77','interserver.net'),(20,'14th June 2014','23.54.72.77','stackoverflow.com'),(21,'14th June 2014','23.54.72.77','interserver.net'),(22,'14th June 2014','23.54.72.77','stackoverflow.com'),(23,'14th June 2014','23.54.72.77','stackoverflow.com'),(24,'14th June 2014','23.54.72.77','google.com'),(25,'14th June 2014','23.54.72.77','stackoverflow.com'),(26,'14th June 2014','23.54.72.77','google.com'),(27,'14th June 2014','23.54.72.77','google.com'),(28,'14th June 2014','23.54.72.77','google.com'),(29,'14th June 2014','23.54.72.77','prothemes.biz'),(30,'14th June 2014','23.54.72.77','bluepay.com'),(31,'14th June 2014','23.54.72.77','codecanyon.net'),(32,'14th June 2014','23.54.72.77','mobikwik.com'),(33,'14th June 2014','23.54.72.77','interserver.net'),(34,'14th June 2014','23.54.72.77','stackoverflow.com'),(35,'14th June 2014','23.54.72.77','google.com'),(36,'4th August 2016','127.0.0.1','google.com'),(37,'5th August 2016','127.0.0.1','google.com'),(38,'5th August 2016','127.0.0.1','google.com'),(39,'5th August 2016','127.0.0.1','google.com'),(40,'5th August 2016','127.0.0.1','google.com'),(41,'5th August 2016','127.0.0.1','google.com'),(42,'5th August 2016','127.0.0.1','google.com'),(43,'5th August 2016','127.0.0.1','google.com'),(44,'5th August 2016','127.0.0.1','google.com'),(45,'5th August 2016','127.0.0.1','google.com'),(46,'5th August 2016','127.0.0.1','google.com'),(47,'5th August 2016','127.0.0.1','google.com'),(48,'5th August 2016','127.0.0.1','google.com'),(49,'5th August 2016','127.0.0.1','google.com'),(50,'5th August 2016','127.0.0.1','google.com'),(51,'5th August 2016','127.0.0.1','google.com'),(52,'5th August 2016','127.0.0.1','google.com'),(53,'5th August 2016','127.0.0.1','google.com'),(54,'5th August 2016','127.0.0.1','google.com'),(55,'6th August 2016','113.190.237.228','google.com'),(56,'6th August 2016','113.190.237.228','google.com'),(57,'8th August 2016','14.162.168.224','google.com'),(58,'8th August 2016','14.162.168.224','google.com'),(59,'8th August 2016','14.162.168.224','google.com'),(60,'8th August 2016','14.162.168.224','google.com'),(61,'8th August 2016','14.162.168.224','google.com'),(62,'8th August 2016','14.162.168.224','google.com'),(63,'8th August 2016','14.162.168.224','google.com'),(64,'8th August 2016','14.162.168.224','google.com'),(65,'8th August 2016','14.162.168.224','google.com'),(66,'8th August 2016','14.162.168.224','bing.com'),(67,'8th August 2016','14.162.168.224','bing.com'),(68,'8th August 2016','14.162.168.224','bing.com'),(69,'8th August 2016','14.162.168.224','bing.com'),(70,'8th August 2016','14.162.168.224','bing.com'),(71,'8th August 2016','14.162.168.224','bing.com'),(72,'8th August 2016','14.162.168.224','google.com'),(73,'8th August 2016','14.162.168.224','google.com'),(74,'8th August 2016','14.162.168.224','apple.com'),(75,'8th August 2016','42.118.190.64','whatismyfuture.net'),(76,'9th August 2016','14.162.168.224','google.com'),(77,'11th August 2016','123.16.229.189','google.com'),(78,'11th August 2016','123.16.229.189','google.com'),(79,'11th August 2016','123.16.229.189','vicoders.com'),(80,'11th August 2016','168.235.85.147','google.com'),(81,'11th August 2016','168.235.85.147','google.com');
/*!40000 ALTER TABLE `user_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-11  9:32:59
