<?php

/*
* @author Balaji
* @Script Name: Turbo SEO Analyzer
* @ By ProThemes.Biz 2014
*/

error_reporting(1);

$filename = 'install.php';

if (file_exists($filename)) {
    echo "Install.php file exists! <br /> <br />  Redirecting to installer panel...";
    header("Location: install.php");
    echo '<meta http-equiv="refresh" content="1;url=install.php">';
    exit();
} 

$banned_ip = "0.0.0.0";
$ban_user = 1;
$banned_site = "example.com";
$ban_site = 1;
$check_site = 1;
require_once ('config.php');
function str_contains($haystack, $needle, $ignoreCase = false)
{
    if ($ignoreCase)
    {
        $haystack = strtolower($haystack);
        $needle = strtolower($needle);
    }
    $needlePos = strpos($haystack, $needle);
    return ($needlePos === false ? false : ($needlePos + 1));
}
$date = date('jS F Y');
$ip = $_SERVER['REMOTE_ADDR'];
$data_ip = file_get_contents('ips.txt');

$con = mysqli_connect($mysql_host, $mysql_user, $mysql_pass, $mysql_database);

if (mysqli_connect_errno())
{
    $sql_error = mysqli_connect_error();
    $check_site = 0;
    goto myoff;
}
$query = "SELECT * FROM site_info";
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $title = Trim($row['title']);
    $des = Trim($row['des']);
    $keyword = Trim($row['keyword']);
    $site_name = Trim($row['site_name']);
    $email = Trim($row['email']);
    $twit = Trim($row['twit']);
    $face = Trim($row['face']);
    $gplus = Trim($row['gplus']);
    $ex_1 = Trim($row['ex_1']);
    $ex_2 = Trim($row['ex_2']);
    $ps_1 = Trim($row['ps_1']);
    $ps_2 = Trim($row['ps_2']);
    $ps_3 = Trim($row['ps_3']);
    $ga = Trim($row['ga']);
}

$sql = "SELECT * FROM lang where id='0'";
$result = mysqli_query($con, $sql);

while ($row = mysqli_fetch_array($result))
{

    $lang = Trim($row['type']);
}
require_once ("langs/$lang");

$query = "SELECT * FROM ban_user";
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $banned_ip = $banned_ip . "::" . $row['ip'];
}
if (strpos($banned_ip, $ip) !== false)
{
    $ban_user = 0;
    goto banned_user;
}

$query = "SELECT * FROM ban_site";
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $banned_site = $banned_site . "::" . $row['site'];
}
if (strpos($banned_site, $site) !== false)
{
    $ban_site = 0;
    goto banned_site;
}
$query = "SELECT * FROM ads WHERE id='1'";
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $text_ads = Trim($row['text_ads']);
    $ads_1 = Trim($row['ads_1']);
    $ads_2 = Trim($row['ads_2']);

}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" type="image/png" href="img/favicon.ico" />

        <!-- social network metas -->
<?php

if (isset($_GET['msite']))
{
    $olddes = $des;
    $site = Trim(htmlentities($_GET['msite']));
    $title = ucfirst($site) . " | $site_name";
    $result = mysqli_query($con, "SELECT * FROM sitemap WHERE site='$site'");
    while ($row = mysqli_fetch_array($result))
    {
        $des = Trim($row['des']);
    }
    if ($des == "")
    {
        $des = $olddes;
    }
}

?>
        <meta property="site_name" content="<?php

echo $site_name;

?>"/>
        <meta property="description" content="<?php

echo $des;

?>" />
        <meta name="description" content="<?php

echo $des;

?>" />
        <meta name="keywords" content="<?php

echo $keyword;

?>" />
        
        <title><?php

echo $title;

?></title>
		<base href="http://audit.vicoders.com/simple-audit-php/">
        <!-- Main style -->
        <link href="css/theme.css" rel="stylesheet" />
        <!-- Font-Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" />

        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery 1.10.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
           
<style> #newhome{ display:none; } #c_alert1{ display:none; } #c_alert2{ display:none; } #preloader{ display:none; }</style>
 <script>
function contactDoc()
{
var xmlhttp;
var user_name = $('input[name=c_name]').val();
var user_email = $('input[name=c_email]').val();
var user_sub = $('input[name=c_subject]').val();
var user_mes = $('textarea[name=email_message]').val();
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    }
  }
$.post("message.php", {name:user_name,email:user_email,subject:user_sub,message:user_mes}, function(results){
if (results == 1) {
     $("#c_alert1").show();
}
else
{
     $("#c_alert2").show();
}
});
}
</script>   
<script>
function showLoading() {
    $("#preloader").show();
    $("#myhome").hide();
}

function hideLoading() {
    $("#preloader").hide();
}
function loadXMLDoc()
{
var xmlhttp;
var strr = $('input[name=url]').val();
strr = strr.toLowerCase(); strr = strr.replace("http://", ""); strr = strr.replace("https://", ""); 
strr = strr.replace("www.", ""); 
var rgx = /^(http(s)?:\/\/)?(www\.)?[a-z0-9\-]{2,100}(\.[a-z]{2,100})(\.[a-z]{2,2})?$/i; 
if (strr==null || strr=="" || !rgx.test(strr)) {
alert("Not a vaild domain name");
return false;
}
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    }
  }
showLoading();
$.post("result.php", {url:strr}, function(results){
    hideLoading();
    $("#index-content").hide();
    $("#content").append(results);
});
}
</script>


<script>
function mshowLoading() {
    $("#newcontent").hide();
    $("#newhome").show();
}

function mhideLoading() {
    $("#newhome").hide();
    $("#newcontent").hide();
}
function mloadXMLDoc()
{
var xmlhttp;
var strr = $('input[name=url]').val();
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    }
  }
mshowLoading();
$.post("result.php", {url:strr}, function(results){
    mhideLoading();
    $("#newcontent1").append(results);
});
}
</script>

<style>
#fountainG{
position:relative;
width:166px;
height:20px}

.fountainG{
position:absolute;
top:0;
background-color:#333333;
width:21px;
height:21px;
-moz-animation-name:bounce_fountainG;
-moz-animation-duration:1.3s;
-moz-animation-iteration-count:infinite;
-moz-animation-direction:linear;
-moz-transform:scale(.3);
-moz-border-radius:14px;
-webkit-animation-name:bounce_fountainG;
-webkit-animation-duration:1.3s;
-webkit-animation-iteration-count:infinite;
-webkit-animation-direction:linear;
-webkit-transform:scale(.3);
-webkit-border-radius:14px;
-ms-animation-name:bounce_fountainG;
-ms-animation-duration:1.3s;
-ms-animation-iteration-count:infinite;
-ms-animation-direction:linear;
-ms-transform:scale(.3);
-ms-border-radius:14px;
-o-animation-name:bounce_fountainG;
-o-animation-duration:1.3s;
-o-animation-iteration-count:infinite;
-o-animation-direction:linear;
-o-transform:scale(.3);
-o-border-radius:14px;
animation-name:bounce_fountainG;
animation-duration:1.3s;
animation-iteration-count:infinite;
animation-direction:linear;
transform:scale(.3);
border-radius:14px;
}

#fountainG_1{
left:0;
-moz-animation-delay:0.52s;
-webkit-animation-delay:0.52s;
-ms-animation-delay:0.52s;
-o-animation-delay:0.52s;
animation-delay:0.52s;
}

#fountainG_2{
left:21px;
-moz-animation-delay:0.65s;
-webkit-animation-delay:0.65s;
-ms-animation-delay:0.65s;
-o-animation-delay:0.65s;
animation-delay:0.65s;
}

#fountainG_3{
left:42px;
-moz-animation-delay:0.78s;
-webkit-animation-delay:0.78s;
-ms-animation-delay:0.78s;
-o-animation-delay:0.78s;
animation-delay:0.78s;
}

#fountainG_4{
left:62px;
-moz-animation-delay:0.91s;
-webkit-animation-delay:0.91s;
-ms-animation-delay:0.91s;
-o-animation-delay:0.91s;
animation-delay:0.91s;
}

#fountainG_5{
left:83px;
-moz-animation-delay:1.04s;
-webkit-animation-delay:1.04s;
-ms-animation-delay:1.04s;
-o-animation-delay:1.04s;
animation-delay:1.04s;
}

#fountainG_6{
left:104px;
-moz-animation-delay:1.17s;
-webkit-animation-delay:1.17s;
-ms-animation-delay:1.17s;
-o-animation-delay:1.17s;
animation-delay:1.17s;
}

#fountainG_7{
left:125px;
-moz-animation-delay:1.3s;
-webkit-animation-delay:1.3s;
-ms-animation-delay:1.3s;
-o-animation-delay:1.3s;
animation-delay:1.3s;
}

#fountainG_8{
left:145px;
-moz-animation-delay:1.43s;
-webkit-animation-delay:1.43s;
-ms-animation-delay:1.43s;
-o-animation-delay:1.43s;
animation-delay:1.43s;
}

@-moz-keyframes bounce_fountainG{
0%{
-moz-transform:scale(1);
background-color:#333333;
}

100%{
-moz-transform:scale(.3);
background-color:#FFFFFF;
}

}

@-webkit-keyframes bounce_fountainG{
0%{
-webkit-transform:scale(1);
background-color:#333333;
}

100%{
-webkit-transform:scale(.3);
background-color:#FFFFFF;
}

}

@-ms-keyframes bounce_fountainG{
0%{
-ms-transform:scale(1);
background-color:#333333;
}

100%{
-ms-transform:scale(.3);
background-color:#FFFFFF;
}

}

@-o-keyframes bounce_fountainG{
0%{
-o-transform:scale(1);
background-color:#333333;
}

100%{
-o-transform:scale(.3);
background-color:#FFFFFF;
}

}

@keyframes bounce_fountainG{
0%{
transform:scale(1);
background-color:#333333;
}

100%{
transform:scale(.3);
background-color:#FFFFFF;
}

}
</style>
</head>
<?php

$query = "SELECT @last_id := MAX(id) FROM page_view";

$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $last_id = $row['@last_id := MAX(id)'];
}

$query = "SELECT * FROM page_view WHERE id=" . Trim($last_id);
$result = mysqli_query($con, $query);

while ($row = mysqli_fetch_array($result))
{
    $last_date = $row['date'];
}

if ($last_date == $date)
{
    if (str_contains($data_ip, $ip))
    {
        $query = "SELECT * FROM page_view WHERE id=" . Trim($last_id);
        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result))
        {
            $last_tpage = Trim($row['tpage']);
        }
        $last_tpage = $last_tpage + 1;

        // Already IP is there!  So update only page view.
        $query = "UPDATE page_view SET tpage=$last_tpage WHERE id=" . Trim($last_id);
        mysqli_query($con, $query);
    } else
    {
        $query = "SELECT * FROM page_view WHERE id=" . Trim($last_id);
        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result))
        {
            $last_tpage = Trim($row['tpage']);
            $last_tvisit = Trim($row['tvisit']);
        }
        $last_tpage = $last_tpage + 1;
        $last_tvisit = $last_tvisit + 1;

        // Update both tpage and tvisit.
        $query = "UPDATE page_view SET tpage=$last_tpage,tvisit=$last_tvisit WHERE id=" .
            Trim($last_id);
        mysqli_query($con, $query);
        file_put_contents('ips.txt', $data_ip . "\r\n" . $ip);
    }
} else
{
    //Delete the file and clear data_ip
    unlink("ips.txt");
    $data_ip = "";

    // New date is created!
    $query = "INSERT INTO page_view (date,tpage,tvisit) VALUES ('$date','1','1')";
    mysqli_query($con, $query);

    //Update the IP!
    file_put_contents('ips.txt', $data_ip . "\r\n" . $ip);

}
?>

<body>   
   <?php

// 	include ('core/header.php');

?>
     <div id="content">
   <div id="index-content">
        <div id="newcontent1"> <div id="newcontent"> <div class="wrapper">  <center>
            <div class="home">
            
                     <div id="preloader">
         
                              <div class="container">
                    <div class="row">


                            <h1>
                               <?php

echo $site_name;

?>
                            </h1>
<h4><?php

//echo $lang['1'] . " " . $site_name . " " . $lang['2'] . " ";
echo $text_ads;
?></h4>
                
<br /><?php

echo $lang['3'];

?> <br /><br />

<div id="fountainG">
<div id="fountainG_1" class="fountainG">
</div>
<div id="fountainG_2" class="fountainG">
</div>
<div id="fountainG_3" class="fountainG">
</div>
<div id="fountainG_4" class="fountainG">
</div>
<div id="fountainG_5" class="fountainG">
</div>
<div id="fountainG_6" class="fountainG">
</div>
<div id="fountainG_7" class="fountainG">
</div>
<div id="fountainG_8" class="fountainG">
</div>
</div>
   
<br />

<div id="xxd">
<?php

echo $text_ads;

?>
</div>
                   
                    </div><!-- /.row -->
                </div>
         
         
         </div>
             <div id="myhome">
                <div class="container">
                    <div class="row">


                            <h1>
                               <?php

echo $site_name;

?>
                            </h1>
<h4><?php

echo $lang['1'] . " " . $site_name . " " . $lang['2'] . " ";

?></h4>
                

                    <div class="input-group"  style="width:70%;">     <form onsubmit="loadXMLDoc(); return false">                                                         
                  <input type="text" placeholder="<?php

echo $lang['4'];

?>" name="url" id="url" class="form-control input-sm" /></form>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-primary" onclick="loadXMLDoc()" ><i class="fa fa-search"></i></button>
                   </div>
                   </div>                  


             <b><?php

echo $lang['5'];

?></b>   <a onclick="document.getElementById('url').value='<?php

echo $ex_1;

?>';loadXMLDoc()" href="#" title="Get <?php

echo $ex_1;

?> details"><?php

echo $ex_1;

?></a>, <a onclick="document.getElementById('url').value='<?php

echo $ex_2;

?>';loadXMLDoc()" href="#" title="Get <?php

echo $ex_2;

?> details"><?php

echo $ex_2;

?></a>  and so on ...
                   
                   
<br /><br />
<div id="xxxd">
<?php

echo $text_ads;

?>
</div>
                   
                    </div><!-- /.row -->
                </div></div>
            </div><!-- /.home -->
            
            <div class="ad animated fadeInDown delay-5">
               
                <div class="container">
                    <div class="row">                        
            
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.ad -->            
                                        
            <div class="features-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-rocket"></i>
                                <h4><?php

echo $lang['6'];

?></h4>
                                <p>
                                <?php

echo $lang['7'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-laptop"></i>
                                <h4><?php

echo $lang['8'];

?></h4>
                                <p>
                                    <?php

echo $lang['9'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-info"></i>
                                <h4><?php

echo $lang['10'];

?></h4>
                                <p>
                                     <?php

echo $lang['11'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-print"></i>
                                <h4><?php

echo $lang['12'];

?></h4>
                                <p>
                                    <?php

echo $lang['13'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                    </div><!-- /.row -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-bolt"></i>
                                <h4><?php

echo $lang['14'];

?></h4>
                                <p>
                                    <?php

echo $lang['15'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-globe"></i>
                                <h4><?php

echo $lang['16'];

?></h4>
                                <p>
                                    <?php

echo $lang['17'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="feature-item animated fadeInUp delay-3">
                                <i class="fa fa-th"></i>
                                <h4><?php

echo $lang['18'];

?></h4>
                                <p><?php

echo $lang['19'];

?>
                                </p>
                            </div>
                        </div><!-- /.item -->

                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.features-menu-->

            <div class="testimonials">
            <div class="container">
<h4 class="page-header">

</h4>
                              <div class="features-menu">
                <div class="container">
                
<?php

echo $ads_1;

?>
                
                 </div>           
                     </div>
 </div> 
            </div><!-- /.testimonials -->

            <div class="more">
                <div class="container">
                    <h2 class="text-center"><?php

echo $lang['20'];

?></h2>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="thumbnail">
                                <a href="http://<?php

echo $ps_1;

?>"><img class="image-overlay" src="temp/<?php

echo Trim($ps_1);

?>.jpg" alt="<?php

echo Trim($ps_1);

?>" /></a>
                                <div class="caption">
                                    <h3><?php

echo ucfirst(Trim($ps_1));

?></h3>
                                    <p>
                                        <a href="http://<?php

echo Trim($ps_1);

?>" class="btn btn-primary" role="button">Visit</a> 
                                         <a href="/<?php echo Trim($ps_1); ?>" class="btn btn-success" role="button">Details</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="thumbnail">
                                <a href="http://<?php

echo Trim($ps_2);

?>"><img class="image-overlay" src="temp/<?php

echo Trim($ps_2);

?>.jpg" alt="<?php

echo Trim($ps_2);

?>" /></a>
                                <div class="caption">
                                    <h3><?php

echo ucfirst(Trim($ps_2));

?></h3>
                                    <p>
                                        <a href="http://<?php

echo Trim($ps_2);

?>" class="btn btn-primary" role="button">Visit</a>
                                         <a href="/<?php echo Trim($ps_2); ?>" class="btn btn-success" role="button">Details</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="thumbnail">
                                <a href="<?php

echo Trim($ps_3);

?>"><img class="image-overlay" src="temp/<?php

echo Trim($ps_3);

?>.jpg" alt="<?php

echo Trim($ps_3);

?>" /></a>
                                <div class="caption">
                                    <h3><?php

echo ucfirst(Trim($ps_3));

?></h3>
                                    <p>
                                        <a href="http://<?php

echo Trim($ps_3);

?>" class="btn btn-primary" role="button">Visit</a> 
                                         <a href="/<?php echo Trim($ps_3); ?>" class="btn btn-success" role="button">Details</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.more -->   
       <?php

include ('core/footer.php');

?>                </div> 

        </div><!--/.wrapper --> 
    <div id="newhome" style="text-align: center;">
                              <div class="container">
                                                          <br /><br /><br />
                    <div class="row">
                            <h1>
                               <?php

echo $site_name;

?>
                            </h1>
<h4><?php

echo $lang['1'] . " " . $site_name . " " . $lang['2'] . " ";

?></h4>
                
<br /><?php

echo $lang['3'];

?> <br /><br />
<center>
<div id="fountainG">
<div id="fountainG_1" class="fountainG">
</div>
<div id="fountainG_2" class="fountainG">
</div>
<div id="fountainG_3" class="fountainG">
</div>
<div id="fountainG_4" class="fountainG">
</div>
<div id="fountainG_5" class="fountainG">
</div>
<div id="fountainG_6" class="fountainG">
</div>
<div id="fountainG_7" class="fountainG">
</div>
<div id="fountainG_8" class="fountainG">
</div>
</div>
   </center>
<br />


<div id="xd">
<?php

echo $text_ads;

?>
</div>
                   
                    </div><!-- /.row -->
                    <br /> <br /> <br /> <br /> <br /> <br /> <br /> 
                </div>
</div>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <!--script src="js/AdminLTE/app.js" type="text/javascript"></script-->

          </div>  </div></div>

<!-- COMPOSE MESSAGE MODAL --> <?php /*
        <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Contact US</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                        <div id="c_alert1">
                              <div class="alert alert-success alert-dismissable">
       <i class="fa fa-check"></i>
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
       <b><?php

echo $lang['21'];

?>!</b> <?php

echo $lang['22'];

?>
       </div>
                        </div>
                        <div id="c_alert2">
                                <div class="alert alert-danger alert-dismissable">
       <i class="fa fa-ban"></i>
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <b><?php

echo $lang['21'];

?></b> <?php

echo $lang['23'];

?>
        </div>
                        </div>
                        <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Name:</span>
                                    <input name="c_name" id="c_name" type="text" class="form-control" placeholder="<?php

echo $lang['24'];

?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Email ID:</span>
                                    <input name="c_email" id="c_email" type="email" class="form-control" placeholder="<?php

echo $lang['25'];

?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Subject:</span>
                                    <input name="c_subject" id="c_subject" type="email" class="form-control" placeholder="<?php

echo $lang['26'];

?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="email_message" id="email_message" class="form-control" placeholder="<?php

echo $lang['27'];

?>" style="height: 120px;"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php

echo $lang['28'];

?></button>

                            <button type="button" onclick="contactDoc()" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> <?php

echo $lang['29'];

?></button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --> */ ?>
        <script type="text/javascript">
parent.AdjustIframeHeight(document.getElementById("content").scrollHeight);
</script>
    </body>
</html>
<?php

mysqli_close($con);

myoff : if ($check_site == "0")
{
    echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="Turbo SEO Analyser"/>
        <meta property="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />
        <meta name="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />

        <title>Offline Site - Turbo SEO Analyser</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Turbo SEO Analyser
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Offline Site</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! SQL ERROR: ' .
        $sql_error . '</h3>
                            <p>
                                Try to fix your site soon. 
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}
banned_user : if ($ban_user == "0")
{
    echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="' . $site_name . '"/>
        <meta property="description" content="' . $des . '" />
        <meta name="description" content="' . $des . '" />

        <title>' . $lang['30'] . ' - ' . $title . '</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ' . $site_name . '
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> ' .
        $lang['31'] . '</a></li>
                        <li class="active">' . $lang['30'] . '</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> ' . $lang['32'] .
        '</h3>
                            <p>
                                ' . $lang['33'] . '<br>
                                ' . $lang['34'] . ' (' . $ip . ').  
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}
banned_site : if ($ban_site == "0")
{
    echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="' . $site_name . '"/>
        <meta property="description" content="' . $des . '" />
        <meta name="description" content="' . $des . '" />

        <title>' . $lang['35'] . ' - ' . $title . '</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ' . $site_name . '
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> ' .
        $lang['31'] . '</a></li>
                        <li class="active">' . $lang['35'] . '</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                          <h3><i class="fa fa-warning text-yellow"></i> ' . $lang['32'] .
        '</h3>
                            <p>
                                ' . $lang['33'] . '<br>
                                ' . $lang['36'] . ' (' . $site . ').  
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';
}

if (isset($_GET['msite']))
{
    $site = Trim(htmlentities($_GET['msite']));
    echo "<script>
document.getElementById('url').value='$site';
loadXMLDoc();
</script>";
}

?>