<?php
/*
 * @author Balaji
 * @name: Turbo SEO Analyzer PHP Script
 * @copyright � 2014 ProThemes.Biz
 *
 */
error_reporting(1);
$banned_ip = "0.0.0.0";
$ban_user = 1;
$check_site = 1;
require_once('config.php');
function str_contains($haystack, $needle, $ignoreCase = false) {
    if ($ignoreCase) {
        $haystack = strtolower($haystack);
        $needle   = strtolower($needle);
    }
    $needlePos = strpos($haystack, $needle);
    return ($needlePos === false ? false : ($needlePos+1));
}
$date = date('jS F Y');
$ip = $_SERVER['REMOTE_ADDR'];
$data_ip = file_get_contents('ips.txt');

 $con = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,$mysql_database);

  if (mysqli_connect_errno())
  {
  $sql_error = mysqli_connect_error();
  $check_site = 0;
  goto myoff;
  }
    $query =  "SELECT * FROM site_info";
    $result = mysqli_query($con,$query);
        
    while($row = mysqli_fetch_array($result)) {
    $title =  Trim($row['title']);
    $des =   Trim($row['des']);
    $keyword =  Trim($row['keyword']);
    $site_name =   Trim($row['site_name']);
    $email =   Trim($row['email']);
    $twit =   Trim($row['twit']);
    $face =   Trim($row['face']);
    $gplus =   Trim($row['gplus']);
    $ex_1 =   Trim($row['ex_1']);
    $ex_2 =   Trim($row['ex_2']);
    $ps_1 =   Trim($row['ps_1']);
    $ps_2 =   Trim($row['ps_2']);
    $ps_3 =   Trim($row['ps_3']);
    $ga =   Trim($row['ga']);
    }
 
$sql = "SELECT * FROM lang where id='0'";
$result = mysqli_query($con, $sql);

while($row = mysqli_fetch_array($result)) {
//populate and display results data in each row
$lang= Trim($row['type']);
}  
require_once("langs/$lang");
    $query =  "SELECT * FROM ban_user";
    $result = mysqli_query($con,$query);
        
    while($row = mysqli_fetch_array($result)) {
    $banned_ip =  $banned_ip."::".$row['ip'];
    }
    if (strpos($banned_ip,$ip) !== false)
    {
        $ban_user = 0;
        goto banned_user;
    }
if (isset($_GET{'page'})) { 
$page_name = trim($_GET['page']);
$sql = "SELECT * FROM pages where page_name='$page_name'";
$result = mysqli_query($con, $sql);

//we loop through each records
while($row = mysqli_fetch_array($result)) {
//populate and display results data in each row
$page_title = $row['page_title'];
$page_content = $row['page_content'];
$last_date = $row['last_date'];
}
}  
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Language" content="en" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" type="image/png" href="img/favicon.ico" />

        <!-- social network metas -->
        <meta property="site_name" content="<?php echo $site_name; ?>"/>
        <meta property="description" content="<?php echo $des; ?>" />
        <meta name="description" content="<?php echo $des; ?>" />
        <meta name="keywords" content="<?php echo $keyword; ?>" />
        
        <title><?php echo $title; ?></title>

        <!-- Main style -->
        <link href="css/theme.css" rel="stylesheet" />
        <!-- Font-Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" />

        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
</script>
 <style> #newhome{ display:none; } </style>
 
<style>   #c_alert1{ display:none; } #c_alert2{ display:none; }</style>
<script>
function contactDoc()
{
var xmlhttp;
var user_name = $('input[name=c_name]').val();
var user_email = $('input[name=c_email]').val();
var user_sub = $('input[name=c_subject]').val();
var user_mes = $('textarea[name=email_message]').val();
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    }
  }
$.post("message.php", {name:user_name,email:user_email,subject:user_sub,message:user_mes}, function(results){
if (results == 1) {
     $("#c_alert1").show();
}
else
{
     $("#c_alert2").show();
}
});
}
</script>   

 <style> #preloader{ display:none; } </style>
</head>

<?php
  
    $query =  "SELECT @last_id := MAX(id) FROM page_view";
    
    $result = mysqli_query($con,$query);
    
    while($row = mysqli_fetch_array($result)) {
    $last_id =  $row['@last_id := MAX(id)'];
    }
    
    $query =  "SELECT * FROM page_view WHERE id=".Trim($last_id);
    $result = mysqli_query($con,$query);
        
    while($row = mysqli_fetch_array($result)) {
    $last_date =  $row['date'];
    }
    
    if($last_date == $date)
    {
         if (str_contains($data_ip, $ip)) 
        {
          $query =  "SELECT * FROM page_view WHERE id=".Trim($last_id);
          $result = mysqli_query($con,$query);
        
        while($row = mysqli_fetch_array($result)) {
        $last_tpage =  Trim($row['tpage']);
            }
        $last_tpage = $last_tpage +1;
        
          // Already IP is there!  So update only page view.
        $query = "UPDATE page_view SET tpage=$last_tpage WHERE id=".Trim($last_id);
        mysqli_query($con,$query);
        }
        else
        {
        $query =  "SELECT * FROM page_view WHERE id=".Trim($last_id);
        $result = mysqli_query($con,$query);
        
        while($row = mysqli_fetch_array($result)) {
        $last_tpage =  Trim($row['tpage']);
        $last_tvisit =  Trim($row['tvisit']);
        }
        $last_tpage = $last_tpage +1;
        $last_tvisit = $last_tvisit +1;
        
        // Update both tpage and tvisit.
        $query = "UPDATE page_view SET tpage=$last_tpage,tvisit=$last_tvisit WHERE id=".Trim($last_id);
        mysqli_query($con,$query);
        file_put_contents('ips.txt',$data_ip."\r\n".$ip); 
        }
    }
    else
    { 
    //Delete the file and clear data_ip
    unlink("ips.txt");
    $data_ip ="";
    
    // New date is created!
    $query = "INSERT INTO page_view (date,tpage,tvisit) VALUES ('$date','1','1')"; 
    mysqli_query($con,$query);
    
    //Update the IP!
    file_put_contents('ips.txt',$data_ip."\r\n".$ip); 
    
    }
    
        $query =  "SELECT * FROM ads WHERE id='1'";
        $result = mysqli_query($con,$query);
        
        while($row = mysqli_fetch_array($result)) {
        $text_ads =  Trim($row['text_ads']);
        $ads_1 =  Trim($row['ads_1']);
        $ads_2 =  Trim($row['ads_2']);
        
        }
    
?>

<body>
  <div id="content">
   <div id="index-content">
       <?php
   include('core/header.php');
   ?>
<br /><br /><br /><br />

<style>
.header {
    padding: 35px 0;
    text-align: center;
    }
    .header h2 {
    font-size: 30px;
    margin: 0;
    }
.contentx {
    margin-left: auto;
    margin-right: auto;
    padding-left: 15px;
    padding-right: 15px;
    width: 87%;
    margin-bottom: 60px;
    }
.box-radius {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #E1E1E1;
    border-radius: 5px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    padding: 70px 30px;
}</style>
<div class="header">
<h2>
<?php  
                    
                    if ($page_title == "")
                    {
                        echo "No Title";
                        
                    }else
                    {
                       echo $page_title; 
                    }
                    
                    
                    
                     ?>
</h2>
</div>

<div class="contentx">
<div class="box-radius">
<div class="row">
<p><?php 
             if ($page_content == "")
             {
                echo "<br /><br /><br /><br /><br /><br /><br /><br /><br />   
  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
             }
             else
             {
             echo $page_content; 
             }
             ?></p></div>
</div>
 
</div>            
     <?php
include('core/footer.php');
?>              </div> 

        </div><!--/.wrapper --> 

        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
      
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

<!-- COMPOSE MESSAGE MODAL -->
        <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Contact US</h4>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                        <div id="c_alert1">
                              <div class="alert alert-success alert-dismissable">
       <i class="fa fa-check"></i>
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
       <b><?php echo $lang['21']; ?>!</b> <?php echo $lang['22']; ?>
       </div>
                        </div>
                        <div id="c_alert2">
                                <div class="alert alert-danger alert-dismissable">
       <i class="fa fa-ban"></i>
       <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <b><?php echo $lang['21']; ?></b> <?php echo $lang['23']; ?>
        </div>
                        </div>
                        <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Name:</span>
                                    <input name="c_name" id="c_name" type="text" class="form-control" placeholder="<?php echo $lang['24']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Email ID:</span>
                                    <input name="c_email" id="c_email" type="email" class="form-control" placeholder="<?php echo $lang['25']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Subject:</span>
                                    <input name="c_subject" id="c_subject" type="email" class="form-control" placeholder="<?php echo $lang['26']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="email_message" id="email_message" class="form-control" placeholder="<?php echo $lang['27']; ?>" style="height: 120px;"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $lang['28']; ?></button>

                            <button type="button" onclick="contactDoc()" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> <?php echo $lang['29']; ?></button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
    </body>
</html>

<?php
mysqli_close($con);
?>

<?php
myoff:
if ($check_site == "0")
{
      echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="Turbo SEO Analyser"/>
        <meta property="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />
        <meta name="description" content="Turbo SEO Analyser - All-in-one SEO Tool" />

        <title>Offline Site - Turbo SEO Analyser</title>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Turbo SEO Analyser
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Offline Site</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! SQL ERROR: '.$sql_error.'</h3>
                            <p>
                                Try to fix your site soon. 
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';  
}
banned_user:
if ($ban_user == "0")
{
      echo ' 
    <html>
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/png" href="img/favicon.ico">

        <!-- social network metas -->
        <meta property="site_name" content="'.$site_name.'"/>
        <meta property="description" content="'.$des.'" />
        <meta name="description" content="'.$des.'" />

        <title>'.$lang['30'].' - '.$title.'</title>
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
       <body class="skin-blue">
    
    <aside class="right-side strech">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        '.$site_name.'
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> '.$lang['31'].'</a></li>
                        <li class="active">'.$lang['30'].'</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                 <br /> <br /> <br />
                    <div class="error-page">
                        <h2 class="headline"></h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> '.$lang['32'].'</h3>
                            <p>
                                '.$lang['33'].'<br>
                                '.$lang['34'].' ('.$ip.').  
                            </p>
                           
                        </div>
                    </div><!-- /.error-page -->

                </section><!-- /.content -->
            </aside>   </body> </html>';  
}
?>