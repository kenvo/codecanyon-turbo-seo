            <footer class="footer">
           <div class="container" >
                    <div class="row">
                        <div class="none" style="float:left;">
                            Copyright &copy; 2014 <a href="http://prothemes.biz">ProThemes.Biz</a>. All rights reserved.
                        </div>
                        <div class="none" style="float:right;">
                            <a href="<?php echo Trim($twit.$mtwit); ?>" class="btn btn-twitter btn-social" data-toggle="tooltip" data-placement="top" title="Twitter Account"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo Trim($face.$mface); ?>" class="btn btn-facebook btn-social" data-toggle="tooltip" data-placement="top" title="Facebook Page"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo Trim($gplus.$mgplus); ?>" class="btn btn-gplus btn-social" data-toggle="tooltip" data-placement="top" title="Google+ Profile"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
                <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $ga; ?>', 'auto');
  ga('send', 'pageview');

</script>
</footer>